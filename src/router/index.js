import Vue from 'vue'
import Router from 'vue-router'
import Connexion from '@/components/Connexion'
import CreerCompte from '@/components/CreerCompte'
import Discussions from '@/components/Discussions'
import Membres from '@/components/Membres'
import DiscussionDetail from '@/components/DiscussionDetail'



Vue.use(Router);

export default new Router({
  routes: [

      {
          path: '/',
          name: 'Connexion',
          component: Connexion
      },
      {
          path: '/CreerCompte',
          name: 'CreerCompte',
          component: CreerCompte
      },
      {
          path: '/Discussions',
          name: 'Discussions',
          component: Discussions
      },
      {
          path: '/Discussion/:discussionId',
          name: 'Discussion',
          component: DiscussionDetail
      },
      {
          path: '/Membres',
          name: 'Membres',
          component: Membres
      }


  ]
})
