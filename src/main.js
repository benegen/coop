// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import {Utils} from './components/mixins/utils.js';



// API KEY = 2fa3482a3a2b41d8a1dcbf0ad6627cdb
Vue.mixin(Utils);

Vue.config.productionTip = false;

Vue.prototype.$bus = new Vue();

store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});

window.axios = axios.create({
    baseURL: 'http://coop.api.netlor.fr/api/',
    params : {
        token : false
    },
    headers: {
        Authorization: 'Token token=050cf12af48242239ffde4f969677663' //API KEY
    }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App/>',
    beforeCreate() {
        this.$store.commit('initialiseStore');
    },
    mounted() {

    }
});
