export const Utils = {
    methods: {
        memberConnected() {
            if(this.$store.state.member === false) {
                return false;
            } else {
                // on met à jour le token
                this.setTokenAxios(this.$store.state.token);
                // on charge les membres dans le store
                this.chargerMembres();
                return true;
            }
        },
        setTokenAxios(token) {
            window.axios.defaults.params.token = token;

        },
        chargerMembres() {
            window.axios.get('members').then(response => {
                this.$store.commit('setMembers', response.data);
            }).catch(error => {
                this.$store.commit('setMembers', []);
                console.log(error.response.data.error + ": Les membres n'ont pas été récupérés" );
            });

        },

    }
}
