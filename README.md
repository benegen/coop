# coop

> Clone de slack en vue JS dans le cadre du cours de dev appli web - serveur

> Toutes les fonctionnalités demandées sont développées.

> Eléments manquants : les messages d'erreurs ne sont pas affichés à l'utilisateur, ils apparaissent pour le moment dans la console du navigateur.

> Réalisé par Bénédicte PIERSON GENEAU
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

